# Lo que has aprendido

Rescatando la definición de la introducción:

> Python es un lenguaje de programación de alto nivel orientado al uso general.
> Fue creado por Guido Van Rossum y publicado en 1991. La filosofía de python
> hace hincapié en la limpieza y la legibilidad del código fuente con una
> sintaxis que facilita expresar conceptos en menos líneas de código que en
> otros lenguajes.
> 
> Python es un lenguaje de tipado dinámico y gestión de memoria automática.
> Soporta múltiples paradigmas de programación, incluyendo la programación
> orientada a objetos, imperativa, funcional y procedural e incluye una extensa
> librería estándar.

Ahora sí que estás en condición de entenderla no sólo para python sino para
cualquier otro lenguaje que se te presente de este modo. Ahora tienes la
habilidad de poder comprender de un vistazo qué te aporta el lenguaje que
tienes delante únicamente leyendo su descripción.

Desgranándola poco a poco, has conocido la sintaxis de python en bastante
detalle y has visto cómo hace uso de las sangrías para delimitar bloques de
código, cosa que otros lenguajes hacen con llaves (`{}`) u otros símbolos.

La facilidad de expresar conceptos complejos en pocas líneas de código puede
verse en las *list comprehensions*, la sentencia `with` y muchas otras
estructuras del sistema. Python es un lenguaje elegante y directo, similar al
lenguaje natural.

El tipado dinámico trata lo que estudiaste en el apartado sobre datos, donde se
te cuenta que las referencias pueden cambiar de tipo en cualquier momento ya
que son los propios valores los que son capaces de recordar qué tipo tienen.

La gestión de memoria automática también se presenta en el mismo apartado,
contándote que python hace uso de un *garbage collector* o recolector de basura
para limpiar de la memoria los datos que ya no usa.

Los diferentes paradigmas de programación no se han tratado de forma explícita
en este documento, más allá de la programación orientada a objetos, que inunda
python por completo. Sin embargo, el apartado sobre funciones adelanta varios
de los conceptos básicos del paradigma de programación funcional: que las
funciones sean ciudadanos de primera clase (*first-class citizens*), el uso de
funciones anónimas (*lambda*) y las *closures*.

Los paradigmas procedural e imperativo son la base para los dos
paradigmas de los que hemos hablado. La programación imperativa implica que se
programa mediante órdenes (el caso de python, recuerda) en lugar de
declaraciones (como puede ser la programación lógica, donde se muestran un
conjunto de normas que el programa debe cumplir). La programación procedural es
un paradigma cuyo fundamento es el uso de bloques de código y su *scope*,
creando funciones, estructuras de datos y variables aunque, a diferencia de la
programación funcional, en la programación procedural no es necesario que las
funciones sean ciudadanos de primera clase y pueden tener restricciones.

Estos dos últimos paradigmas, en realidad, se soportan casi por accidente al
habilitar los dos anteriores.

En muchas ocasiones, te encontrarás escribiendo pequeñas herramientas y no
necesitarás mucho más que usar las estructuras básicas de python y varias
funciones para alterarlas, por lo que estarás pensando de forma procedural
accidentalmente.

Los paradigmas no son más que patrones de diseño que nos permiten clasificar
los lenguajes y sus filosofías, pero son muy interesantes a la hora de diseñar
nuestras aplicaciones.

Además de todo esto, has tenido ocasión de conocer de forma superficial la
librería estándar del lenguaje y un conjunto de librerías adicionales que te
aportan los puntos de los que la librería estándar carece. Ahora sabes instalar
dependencias y usarlas en entornos virtuales (*virtual environments*) para
mantener limpia tu instalación.

A parte de lo mencionado en la definición del lenguaje, has aprendido a
ejecutar, cargar y distribuir módulos de python, algo primordial si pretendes
crear paquetes o nuevas librerías y usar las de terceros.

Con todo esto, tienes una visión general pero bastante detallada a nivel
técnico de lo que python aporta y cómo. Lo que necesitas para compensarla es
trabajar con él, acostumbrarte a su ecosistema y leer mucho código de buena
calidad para acostumbrarte a seguir las convenciones y recetas habituales.


## El código pythónico

A lo largo del documento se tratan temas que puede que no te esperases
encontrar al leer sobre programación, ya que tu interés principal es resolver
tus problemas de forma efectiva y construir aplicaciones. Hacer robots que te
hagan la vida más fácil, en definitiva.

Sin embargo, quien se dedica a la programación tiene una vida muy ligada a la
vida de quien se dedica a la filosofía o al diseño y es por eso que esas dos
disciplinas aparecen de vez en cuando en cualquier conversación un poco seria
sobre el trabajo con software.

Las tres disciplinas, en primer lugar, ocurren en la mente de las personas y no
en sus manos. Es por eso que los patrones mentales y los modos de pensamiento
son parte fundamental de ellas. Ninguno de ellos son trabajos para los que se
pueda entrenar una memoria muscular. Es necesario pensar. Y es necesario pensar
de forma consciente y premeditada.

Ver cómo desarrollan otras personas su actividad es valioso para realizar tu
tarea con elegancia.

Otro detalle que has debido de observar, sobre todo porque acaba de aparecer,
es la *elegancia*. La elegancia es, hasta cierto punto, subjetiva y depende del
gusto de quien la mira. Sin embargo, esto sólo es así hasta cierto punto, la
realidad es que alguien puede considerar algo elegante y aun así no gustarle.
Python es un ejemplo de algo así. Guste o no guste, python es un lenguaje de
programación elegante, cuya elegancia forma parte primordial de la filosofía
del lenguaje.

El autor de este documento, por ejemplo, no es un entusiasta de python, pero a
lo largo de la travesía de escribir este documento ha podido reencontrarse, una
vez más, con su elegancia.

El concepto del *código pythónico* (*pythonic code*) es un resultado de esto.
Cuando se habla de código pythónico, se habla de un código que sigue los
estándares de elegancia de python. Que es bonito, comprensible y claro. Un
código que la comunidad de desarrollo de python aprobaría.

Cuando programes en python, trata de programar código pythónico, pues es esa la
verdadera razón por la que se creó el lenguaje y es la forma en la que el
lenguaje más fácil te lo va a poner.
