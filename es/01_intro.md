# Introducción

Python es un lenguaje de programación de alto nivel orientado al uso general.
Fue creado por Guido Van Rossum y publicado en 1991. La filosofía de python
hace hincapié en la limpieza y la legibilidad del código fuente con una
sintaxis que facilita expresar conceptos en menos líneas de código que en otros
lenguajes.

Python es un lenguaje de tipado dinámico y gestión de memoria automática.
Soporta múltiples paradigmas de programación, incluyendo la programación
orientada a objetos, imperativa, funcional y procedural e incluye una extensa
librería estándar.

Pronto entenderás lo que todo esto significa, pero antes hay que instalar las
herramientas necesarias y trastear con ellas.

## Instalación

Para trabajar con python se necesita:

- python3: el intérprete de python, en su versión 3. Verás que hay muchas
  subversiones. Este documento cubre cualquiera de ellas.

- pip: el gestor de paquetería de python. También se conoce como pip3 para
  diferenciarlo del pip de python2.

Nosotros añadiremos un par de amigos a la lista:

- idle3: un editor de código python muy sencillo. Usaremos este porque
  representa el ecosistema de forma muy simple. En el futuro, te recomiendo
  usar algún otro editor más avanzado.

- pipenv: el estándar de facto para gestionar entornos virtuales en python3.
  Luego entenderás qué es eso.


### Instalación en distribuciones de Linux

La instalación puede realizarse desde el gestor de paquetes habitual, ya que
python suele distribuirse en todos los repositorios de paquetes.

En las distribuciones que usan el sistema de paquetes de Debian, puede
instalarse desde la terminal con el siguiente comando:

``` bash
sudo apt-get install python3 python3-pip idle3
```

### Instalación en otros sistemas

Como siempre instalar en otros sistemas es más farragoso. Pero no es demasiado
difícil en este caso. La instalación puede realizarse con una descarga desde la
página web oficial de python:

<https://python.org/downloads/>

Una vez ahí seleccionar la versión necesaria, descargar el instalador y seguir
las instrucciones de éste. Recuerda seleccionar **instalar pip** entre las
opciones y activar la casilla de **añadir python al PATH**, que permitirá que
que ejecutes programas de python sin problemas. También puedes añadir
**IDLE**, el programa que sirve para editar el código, pero te recuerdo que
es un programa muy sencillo, que nos servirá para entender lo básico del
entorno sin ocultarnos el proceso, pero que más adelante podrás utilizar otros
editores que simplifiquen tareas.


## Admira el paisaje

Una vez que has instalado python, es interesante ver lo que eso significa.
Python es un intérprete de código fuente del lenguaje del mismo nombre.
Concretamente, la que has instalado es una de las posibles implementaciones (la
implementación de referencia, en este caso) de este intérprete, conocida como
CPython, en su versión 3. Existen otras implementaciones, cada una con sus
peculiaridades, pero ésta es la principal y la más usada.

Como intérprete que es, python es capaz de leer un archivo escrito en su
lenguaje y ejecutar sus órdenes en tu computadora. Ésta es principalmente su
labor. Sin embargo, también es capaz de realizar esta operación de forma
interactiva recibiendo las órdenes una por una y devolviendo el resultado de su
ejecución como respuesta. Este proceso se conoce como REPL, acrónimo de
read-eval-print-loop (lee-evalúa-imprime-repite), aunque en otros lugares se le
conoce como la shell de python.

> NOTA: La shell de python (o REPL) y la shell del sistema son cosas
> diferentes. La shell de sistema también es un intérprete pero del lenguaje
> que el sistema ha definido (Bash, PowerShell...) y no suele ser capaz de
> entender python.

Para acostumbrarte a la shell te propongo que abras IDLE. Lo primero que verás
será parecido a esto:

``` python
Python 3.6.8 (default, Oct  7 2019, 12:59:55) 
[GCC 8.3.0] on linux
Type "help", "copyright", "credits" or "license()" for more information.
>>> 
```

Todo lo que escribas tras el símbolo `>>>` será interpretado como una orden y
cuando la termines pulsando la tecla `ENTER` de tu teclado, recibirás el
resultado de la ejecución de la orden insertada. El acrónimo REPL define el
comportamiento de este ciclo a las mil maravillas:

> NOTA: En este documento, siempre que veas el símbolo `>>>` significa que se
> trata de un ejemplo ejecutado en la REPL. Si no lo ves, se tratará del
> contenido de un archivo de código python ejecutado de forma independiente.

1. Lee lo que introduces.
2. Lo evalúa, obteniendo así un valor como resultado.
3. Lo imprime.
4. Repite el ciclo volviendo a leer.

Por tanto, si introduces un valor directamente será devuelto:

``` python
>>> 1
1
```

Y si lo alteras, por ejemplo, con una operación matemática sencilla, devuelve
el resultado correspondiente:

``` python
>>> 2+2
4
```

Como ejercicio te propongo lo siguiente:

1. Abre la shell de python (puedes hacerlo en IDLE o desde la shell de sistema
   ejecutando `python` o `python3`).
2. Entra en la ayuda interactiva. PISTA: el mensaje que aparece al abrir la
   REPL te dice cómo.
3. Sal de la ayuda (descubre tú mismo cómo se hace).
4. Ejecuta `import this` y lee el resultado.


### Tu primer archivo de código fuente

La REPL es interesante para probar y depurar tus programas (o para usarla como
calculadora), pero es necesario grabar tus programas en ficheros si quieres
poder volver a ejecutarlos más adelante o compartirlos.

En IDLE puedes abrir un nuevo documento de código en el menú de archivo. Una
vez lo tengas, como aún no sabes python puedes introducir lo siguiente:

``` python
nombre = "Guido"
print("Hola, " + nombre)
```

Si guardas el fichero y pulsas `F5` (Ejecutar módulo), verás que en la pantalla
de la REPL aparece el resultado `Hola, Guido`.

Como ves, el resultado de ejecutar los ficheros de código fuente aparece en la
shell, pero únicamente aparece lo que explícitamente le has pedido que imprima
con la orden `print`.

Para entender el valor de la REPL, te sugiero que vayas a su ventana, y justo
después del resultado de la ejecución hagas lo siguiente:

``` python
Hola, Guido
>>> nombre
'Guido'
>>>
```

La REPL conoce el valor `nombre` a pesar de que tu programa ha terminado de
ejecutarse. Esto es interesante a la hora de probar y analizar el programa.


### La vida real

En realidad, los programas de producción no se ejecutan en una shell como la
que IDLE nos brinda. IDLE sólo está facilitando nuestro trabajo como
desarrolladores, como otros entornos de desarrollo hacen, cada uno a su manera.
En producción el código se levantará ejecutando el intérprete de python
directamente con nuestro programa como input. Por ejemplo en la shell **de
sistema** usando el siguiente comando.

``` bash
python ejemplo.py
```

También es posible ejecutar los programas de python desde la interfaz gráfica,
pero internamente el resultado será el mismo. Siempre que todo esté bien
instalado y configurado, el sistema operativo despertará un intérprete de
python que ejecute las órdenes del fichero.

Es importante ser consciente de lo que ocurre bajo la alfombra, para así ser
capaces de intervenir si encontramos errores.

Más adelante, en la sección sobre módulos e importación volveremos aquí y
estudiaremos cómo se cargan y se interpretan los programas.


## Lo que has aprendido

Has instalado python y te has acostumbrado a la herramienta (IDLE) que usarás
durante tu aprendizaje. Has ejecutado tu primer fichero y encontrado la
potencia de la REPL.

Además, has abierto la ayuda y te has leído el Zen de Python, que pronto iremos
desgranando juntos.

Para ser una introducción no está nada mal.
