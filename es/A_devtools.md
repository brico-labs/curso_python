# Anexo I: Herramientas de desarrollo {-}

IDLE es una herramienta de desarrollo muy limitada, suficiente para seguir los
ejemplos que se recogen en este documento pero insuficiente para desarrollar
aplicaciones avanzadas.

## Desarrollo de código fuente

Existen gran cantidad de entornos de desarrollo (IDE) avanzados y editores que
pueden ser recomendables, aunque es cuestión de gusto personal decantarse por
uno de ellos o por otro.

La diferencia entre un entorno de desarrollo integrado y un editor es la
siguiente: los entornos de desarrollo cumplen varias funciones adicionales,
como en el caso de IDLE, dar acceso a una REPL de python y la posibilidad de
analizar las variables en memoria. Los editores únicamente sirven para escribir
el código, aunque en muchos casos la línea que separa ambos conceptos es
bastante borrosa: existen editores con funcionalidades avanzadas y entornos
integrados muy sencillos que parecen un simple editor. Resumiendo, los entornos
integrados de desarrollo (IDE *integrated development environment*) tienen
editores entre sus herramientas.

### Entornos de desarrollo integrados

Quien no utiliza entornos de desarrollo avanzados (una cuestión de gusto
personal), por lo que se le hace difícil recomendar alguno en particular. Sin
embargo, el wiki de python recoge una larguísima lista de editores y entornos
de desarrollo integrado interesantes[^ides].

[^ides]: <https://wiki.python.org/moin/PythonEditors>

En el entorno del análisis de datos, la distribución *Anaconda* es muy usada.
Anaconda es más que un entorno de desarrollo integrado. Incluye un entorno de
desarrollo llamado Spyder, una shell propia, un gestor de paquetes y
dependencias propio llamado Conda, posibilidad de integración con el lenguaje
de programación R y gran cantidad de paquetes instalados por defecto.

En otros entornos PyCharm es bastante común, aunque también son muy comunes los
entornos de desarrollo integrados pensados para otros lenguajes que han
comenzado a soportar python posteriormente como KDevelop, NetBeans y otros.

Te recomiendo que, si usas un entorno de desarrollo integrado en otros
lenguajes, investigues si soporta python. De este modo no tendrás que aprender
una nueva herramienta. Al ser un lenguaje tan común, probablemente lo soporte.
Si no lo soporta prueba con un IDE que siga una filosofía similar al que uses.

### Editores de código

Quien te escribe usa Vim, un editor de texto muy antiguo con muchas
características que le hacen ser un editor muy eficiente. Existe gran variedad
de editores de código que recomendar: Emacs, gEdit, Kate, Sublime, Atom... Todo
dependerá de tus gustos personales.

La ventaja principal de los editores de código es que conociendo uno en
profundidad es más que suficiente para cualquier lenguaje, ya que están
diseñados únicamente para escribir el contenido de tus programas, dejando las
peculiaridades de ejecución de cada lenguaje a parte. Esto les aporta una
ligereza difícilmente alcanzable por los IDEs.

Sin embargo, esta virtud también es su mayor defecto. Al no integrar ninguna
herramienta adicional, es necesario trabajar todo manualmente. En el caso de
python, te fuerzan a usar una shell independiente y a interactuar con ella de
forma manual. Al principio puede ser tedioso, pero aprender a gestionar los
detalles manualmente es interesante ya que te permite obtener un gran
conocimiento del sistema y lenguaje en el que trabajas.

## Herramientas de depuración

El propio diseño de python permite que sea fácilmente depurable. La REPL
facilita que se pruebe la aplicación a medida que se va desarrollando, función
a función, para asegurar que su comportamiento es el correcto. Además, la
capacidad introspectiva del lenguaje es una buena herramienta, en conjunción
con la REPL para comprobar el comportamiento.

Además de estas características, la instalación de python viene acompañada del
programa `pdb` (*Python Debugger*), un depurador de código similar al conocido
[GNU-Debugger](https://en.wikipedia.org/wiki/GNU_Debugger). Existen otros
depuradores de código más amigables que este, pero la realidad es que no suelen
ser necesarios.

## Testeo de aplicaciones

Hoy en día el testeo de software es muy común, en parte gracias al desarrollo
guiado por pruebas, o TDD (*Test Driven Development*).

La librería estándar de python incluye un módulo para pruebas unitarias llamado
`unittest` en que la librería Nose, muy conocida y usada, se basa para
facilitar el trabajo de modo similar a lo que ocurre con Requests y `urllib`.

Por supuesto, existen otras alternativas, pero estas son las principales.
