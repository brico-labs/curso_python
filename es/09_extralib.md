# Librerías útiles

Ahora que ya sabes cómo instalar librerías y que has visto que muchas
funcionalidades están contenidas en la librería estándar de python es un buen
momento para que visites varios proyectos que aportan recursos muy interesantes
a la hora de resolver problemas. Debido al carácter de uso general de python,
estas librerías aportan facilidades muy diversas. El criterio para escogerlas
parte de la experiencia personal del autor de este documento y añade algunas
librerías y herramientas que pueden ser interesantes debido a su amplio uso en
la industria.

## Librerías científicas: ecosistema SciPy

SciPy es un ecosistema de librerías de cálculo que tiene como objetivo
facilitar la tarea de ingenieros, científicos y matemáticos en sus
respectivos trabajos. Puedes considerarlo como un **Matlab** para python
separado por componentes independientes.

Además de ser el nombre del ecosistema, comparte nombre con una de las
librerías fundamentales de éste. El ecosistema está formado por varias
librerías, entre ellas se encuentran:

- Numpy: un paquete de computación científica para python. Soporta matrices
  multidimensionales, funciones sofisticadas y álgebra lineal, entre otras.

- SciPy: librería basada en Numpy que aporta rutinas numéricas eficientes para
  interpolación, optimización, algebra lineal, estadística y otros.

- SymPy: una librería de matemática simbólica para python que complementa el
  resto de librerías del ecosistema, ya que casi todas están orientadas al
  análisis numérico.

- Matplotlib: librería para representar gráficos y figuras científicas en 2D.

- Pandas: aporta unas estructuras de datos muy potentes basadas en tablas, su
  objetivo es reforzar a python a la hora de tratar datos. El área de esta
  librería es el del análisis de datos, pero puede combinarse con otras áreas
  de estudio como la econometría (ver el proyecto statsmodels). Esta librería
  dispone de un ecosistema muy poderoso a su alrededor debido al auge del
  análisis de datos en la industria del software. Muchas librerías comparten la
  interfaz de Pandas por lo que tener nociones de su comportamiento abre muchas
  puertas en el sector del análisis de datos. Al igual que ocurre en SciPy, Las
  estructuras de datos de Pandas también se basan en Numpy.

- IPython: más que una librería, IPython es una herramienta. Se trata de una
  REPL interactiva (su propio nombre viene de *Interactive Python*) que añade
  diversas funcionalidades sobre la REPL de python habitual: histórico de
  comandos, sugerencias, comandos mágicos (*magic*) que permiten alterar el
  comportamiento de la propia interfaz y un larguísimo etcétera. IPython,
  además, sirve como núcleo de los cuadernos Jupyter que integran una shell de
  python con visualización de tablas y gráficos para generar documentos estilo
  *literate programming*.

## Machine Learning: ScikitLearn

ScikitLearn es una librería de machine learning muy potente, implementa gran
cantidad de algoritmos y tiene una documentación extensa, sencilla y educativa.

Encaja a la perfección con el ecosistema SciPy, ya que se basa en NumPy, SciPy
y Matplotlib.

## Peticiones web: Requests

Requests es una librería alternativa al módulo `urllib` aportado por la
librería estándar de python. Se describe a sí misma como «HTTP para seres
humanos», sugiriendo que `urllib` no es cómoda de usar.

Requests gestiona automáticamente las URL-s de las peticiones a partir de los
datos que se le entreguen, sigue redirecciones, almacena cookies, descomprime
automáticamente, decodifica las respuestas de forma automática, etc. En general
es una ayuda cuando no se quiere dedicar tiempo a controlar cada detalle de la
conexión de modo manual.

## Manipulación de HTML: Beautifulsoup

Beautifulsoup es una librería de procesado de HTML extremadamente potente.
Simplifica el acceso a campos de ficheros HTML con una sintaxis similar a los
objetos de python, permitiendo acceder a la estructura anidada mediante el uso
del punto en lugar de tener que lidiar con consultas extrañas o expresiones
regulares. Esta funcionalidad puede combinarse con selectores CSS4 para un
acceso mucho más cómodo.

Muy usada en conjunción con Requests, para el desarrollo de aplicaciones de
web-scraping. Aunque, si se desea usar algo más avanzado, se recomienda usar el
framework Scrapy.

## Tratamiento de imagen: Pillow

Pillow es un fork de la librería PIL (*Python Imaging Library*) cuyo desarrollo
fue abandonado. En sus diversos submódulos, Pillow permite acceder a datos de
imágenes, aplicarles transformaciones y filtros, dibujar sobre ellas, cambiar
su formato, etc.

## Desarrollo web: Django, Flask

Existen infinidad de frameworks y herramientas de desarrollo web en python. Así
como en el caso del análisis de datos la industria ha convergido en una
herramienta principal, Pandas, en el caso del desarrollo web hay muchas
opciones donde elegir.

Los dos frameworks web más usados son Django y Flask, siendo el segundo menos
común que el primero pero digno de mención en conjunción con el otro por varias
razones.

La primera es la diferencia en la filosofía: así como Django decide con qué
herramientas se debe trabajar, Flask, que se define a sí mismo como
microframework, deja en manos de quien lo usa la elección de qué herramientas
desea aplicarle. Cada una de las dos filosofías tiene ventajas y desventajas y
es tu responsabilidad elegir las que más te convengan para tu proyecto.

La segunda razón para mencionar Flask es que su código fuente es uno de los más
interesantes a la hora de usar como referencia de cómo se debe programar en
python. Define su propia norma de estilo de programación, basada en la
sugerencia de estilo de python[^pep8] y su desarrollo es extremadamente
elegante.

Django, por su parte, ha sido muy influyente y muchas de sus decisiones de
diseño han sido adoptadas por otros frameworks, tanto en python como en otros
lenguajes. Lo que sugiere que está extremadamente bien diseñado.

A pesar de las diferencias filosóficas, existen muchas similitudes entre ambos
proyectos por lo que aprender a usar uno de ellos facilita mucho el uso del
otro y no es aprendizaje perdido. No tengas miedo en lanzarte a uno.

[^pep8]: <https://www.python.org/dev/peps/pep-0008/>


## Protocolos de red: Twisted

Twisted es motor de red asíncrono para python. Sobre él se han escrito
diferentes librerías para gestión de protocolos de Internet como DNS, via
Twisted-Names, IMAP y POP3, via Twisted-Mail, HTTP, via Twisted-Web, IRC y
XMPP, via Twisted-Words, etc.

El diseño asíncrono del motor facilita sobremanera las comunicaciones
eficientes. Programar código asíncrono en python es relativamente sencillo,
pero ha preferido dejarse fuera de este documento por diversas razones. Te
animo a indagar en esta libreria para valorar el interés del código asíncrono.

## Interfaces gráficas: PyQt, PyGTK, wxPython, PySimpleGUI

A pesar de que python dispone de un módulo en su librería estándar para tratar
interfaces gráficas llamado TKinter, es recomendable utilizar librerías más
avanzadas para esto. TKinter es una interfaz a la herramienta Tk del lenguaje
de programación Tcl y acompaña a python desde hace años.

En programas simples TKinter es más que suficiente (IDLE, por ejemplo, está
desarrollado con TKinter) pero a medida que se necesita complejidad o capacidad
del usuario para configurar detalles de su sistema suele quedarse pequeño.

Para programas complejos se recomienda usar otro tipo de librerías más
avanzadas como PyQt, PyGTK o wxPython, todas ellas interfaces a librerías
escritas en C/C++ llamadas Qt, GTK y wxWidgets respectivamente. Estas librerías
aportan una visualización más elegante, en algunos casos usando widgets nativos
del sistema operativo en el que funcionan.

Debido a la complejidad del ecosistema nace el proyecto PySimpleGUI, que
pretende aunar las diferentes herramientas en una sola, sirviendo de interfaz a
cualquiera de las anteriores y alguna otra. Además, el proyecto aporta gran
cantidad de ejemplos de uso. PySimpleGUI aún está en desarrollo y el soporte de
algunos de los motores no está terminado, pero es una fuente interesante de
información y recursos.
